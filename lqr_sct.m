% LQR for an SCT model on adaptive intervention for physical
% activity
% Copyright - Andrei Marchidan
clear all
close all
clc

% -------------------------------------------------------------------------
% constant parameters
tau2    = 10; 
tau3    = 30; 
tau4    = 1;%0.8; 
tau5    = 2; 
tau6    = 0.5;

gamma_311   = 0.4; 
gamma_29    = 2.5; 
gamma_57    = 1; 
gamma_510   = 0.6; 
gamma_64    = 1.5;

beta_25     = 0.5; 
beta_34     = 0.2; 
beta_42     = 0.3;
beta_43     = 0.9;
beta_45     = 0.5; 
beta_46     = 0.9;
beta_54     = 0.6;

K_sr        = 0.8;
lambda      = 1;

c_sr        = 1;

% -------------------------------------------------------------------------
% coefficient matrices
n_x         = 5;        % # of states
n_u         = 3;        % # of inputs
n_y         = 4;        % # of outputs

A = [...
    (1-1/tau2),        0,                  0,                                      beta_25*1/tau2,            0;
    0,                  (1-1/tau3),        beta_34*1/tau3 + gamma_311*1/tau3,    0,       0;
    beta_42*1/tau4,    beta_43*1/tau4,    (1-1/tau4),                            beta_45*1/tau4,            beta_46*1/tau4;
    0,                  0,                  beta_54*1/tau5,                        (1-1/tau5),                0;
    0,                  0,                  0 - gamma_64*c_sr*1/tau6,              0,  (1-1/tau6)];

B = [...
    0,                      gamma_29*1/tau2,   0;
    -gamma_311*1/tau3,     0,                  0;
    0,                      0,                  0;
    0,                      0,                  gamma_510*1/tau5;
    gamma_64*c_sr*1/tau6,  0,                  0];
C = eye(4);
C = horzcat(C,zeros(4,1));
D = zeros(n_y,n_u);

Q = zeros(n_x); Q(3,3) = 1;
R = eye(n_u);
[K] = lqr(A,B,Q,R);

% -------------------------------------------------------------------------
% initial and desired conditions
xi      = [0;0;5000;0;0];
xref    = [0;0;6000;0;0];

% -------------------------------------------------------------------------
% Simulate Dynamics
N = 50;
dt = .1;

% Get discretized system matrices
sys = ss(A,B,C,D);
sys_d = c2d(sys,dt);

% Compute optimal control gain K
[X,L,G] = dare(sys_d.A,sys_d.B,Q,R);
K = inv(R+sys_d.B'*X*sys_d.B)*sys_d.B'*X*sys_d.A;

% Find feedforward term
[uref,xref] = pinv_cvx(A,B,xref,[0;0;0]);
xi = xref; xi(3) = 5000;
xt{1} = xi;

i_end = 0;
while xi(3)<9000
    if i_end > 0
        [uref,xref] = pinv_cvx(A,B,xref,uref);
    end
    
    eta4(1+i_end) = xi(3);
    count = 0;
    for i = 1:N/dt-1
        u{i+i_end} = (-K*(xt{i+i_end}-xref)+uref);

        if i>1
            if norm(xt{i+i_end}(3)-xref(3))<300
                count = count+1;
            else
                count = 0;
            end
        end
        
        xt{i+1+i_end} = sys_d.A*xt{i+i_end}+sys_d.B*u{i+i_end};
        eta4(i+1+i_end) = xt{i+i_end+1}(3);
        u8(i+i_end) = u{i+i_end}(1);
        u9(i+i_end) = u{i+i_end}(2);
        u10(i+i_end) = u{i+i_end}(3);
        if count > 10 && xt{i+i_end}(3)<9000 %|| i==N/dt-1
            xi = xt{1+i+i_end};
            xref = [0;0;xref(3)+1000;0;0];
            i_end = i_end+i;
            break;
        elseif i==N/dt-1
            xi = xt{1+i+i_end};
            i_end = i_end+i;
            break;
        end
    end
end

tscale = [1:length(eta4)]*0.1;
figure();
subplot(2,1,1);
plot(tscale,eta4(:));
hold on;
plot(tscale,ones(1,length(eta4))*6000,'--k');
plot(tscale,ones(1,length(eta4))*7000,'--k');
plot(tscale,ones(1,length(eta4))*8000,'--k');
plot(tscale,ones(1,length(eta4))*9000,'--k');
plot(tscale,ones(1,length(eta4))*10000,'--k');
subplot(2,1,2);
plot([1:length(u8)]*0.1,u8);
hold on;
plot([1:length(u9)]*0.1,u9,'r');
plot([1:length(u10)]*0.1,u10,'m');