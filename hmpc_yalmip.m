% Implementation of HMPC for SCT model on adaptive intervention for
% physical activity
yalmip('clear');
clear all
close all
clc

% Model data
% -------------------------------------------------------------------------
% constant parameters
tau2    = 10; 
tau3    = 30; 
tau4    = 0.8; 
tau5    = 2; 
tau6    = 0.5;

gamma_311   = 0.4; 
gamma_29    = 2.5; 
gamma_57    = 1; 
gamma_510   = 0.6; 
gamma_64    = 1.5;

beta_25     = 0.5; 
beta_34     = 0.2; 
beta_42     = 0.3;
beta_43     = 0.9;
beta_45     = 0.5; 
beta_46     = 0.9;
beta_54     = 0.6;

K_sr        = 0.8;
lambda      = 1;

c_sr        = 1;

% -------------------------------------------------------------------------
% initial and desired conditions
xi      = [0;0;5000;0;0];
yref    = [0;0;10000;0];
uref    = [0;0;0];

z8 = [5000,6000,7000,8000,9000,10000]';
z9 = [100,200,300,400,500]';
zk = diag([z8;z9]);

% -------------------------------------------------------------------------
% solver settings
h_p         = 10;        % prediction horizon
h_c         = 5;        % control horizon
dt          = 1;        % sampling time (1 day)
disc_sz     = h_p/dt;   % discretized size

n_x         = 5;        % # of states
n_u         = 3;        % # of inputs
n_y         = 4;        % # of outputs

n_u8        = 6;        % # of possible values for u8
n_u9        = 5;        % # of possible values for u9

tol         = 600;      % tolerance in # of daily steps

% Constraints data
umin        = [5000,0,0]';
umax        = [10000,500,500]';

ymin        = [0,0,0,0]';
ymax        = [10000,10000,12000,10000]';

dumin       = [-1000, -500, -500]';
dumax       = [1000, 500, 500]';

% -------------------------------------------------------------------------
% coefficient matrices
A = [...
    (1-dt/tau2),        0,                  0,                                      beta_25*dt/tau2,            0;
    0,                  (1-dt/tau3),        beta_34*dt/tau3 + gamma_311*dt/tau3,    0,       0;
    beta_42*dt/tau4,    beta_43*dt/tau4,    (1-dt/tau4),                            beta_45*dt/tau4,            beta_46*dt/tau4;
    0,                  0,                  beta_54*dt/tau5,                        (1-dt/tau5),                0;
    0,                  0,                  0 - gamma_64*c_sr*dt/tau6,              0,  (1-dt/tau6)];

B = [...
    0,                      gamma_29*dt/tau2,   0;
    -gamma_311*dt/tau3,     0,                  0;
    0,                      0,                  0;
    0,                      0,                  gamma_510*dt/tau5;
    gamma_64*c_sr*dt/tau6,  0,                  0];


% -------------------------------------------------------------------------
% Augmented matrices
C = eye(4);
C = horzcat(C,zeros(4,1));
D = zeros(n_y,n_u);
sys = ss(A,B,C,D);
sys_d = c2d(sys,dt);
A = sys_d.A;
B = sys_d.B;
C = sys_d.C;
D = sys_d.D;


IE1 = [...
    umin(1)-ymax(3);
    umax(1)-ymin(3);
    umax(2)-umin(3);
    umax(3)-umin(2);
    umin(3);
    -umax(3)];

IE2 = [0,0,-1,1,-1,1]';
IE3 = [...
        0;
        umax(1)-ymin(3);
        umax(2)-umin(3);
        umax(3)-umin(2);
        0;
        0];
IE4 = [1,0,0; -1,0,0; 0,-1,0; 0,1,0; 0,0,0; 0,0,0];
IE5 = [0,0,-1,0; 0,0,1,0; zeros(4)];
IE6 = eye(n_u);
IE7 = eye(n_u);

E1 = [1,0,0;0,1,0];
E2 = [ones(1,n_u8),zeros(1,n_u9); zeros(1,n_u8), ones(1,n_u9)];
E3 = [1;1];


% -------------------------------------------------------------------------
% States x
x = sdpvar(repmat(n_x,1,disc_sz),repmat(1,1,disc_sz));
% Outputs y
y = sdpvar(repmat(n_y,1,disc_sz),repmat(1,1,disc_sz));
% Inputs u 
u = sdpvar(repmat(n_u,1,disc_sz),repmat(1,1,disc_sz));
% Aux vars
z = sdpvar(repmat(n_u8+n_u9,1,disc_sz),repmat(1,1,disc_sz));
z10 = sdpvar(repmat(1,1,disc_sz),repmat(1,1,disc_sz));
% Binary vars
del = binvar(repmat(n_u8+n_u9,1,disc_sz),repmat(1,1,disc_sz));
del10 = binvar(repmat(1,1,disc_sz),repmat(1,1,disc_sz));


% -------------------------------------------------------------------------
constraints = [];
objective = norm(y{disc_sz}-yref,2);

for k = disc_sz-1:-1:1  

    constraints = [constraints , ymin <= y{k},...
                                y{k} <= ymax,...
                                umin <= u{k},...
                                u{k} <= umax,...
                                E1*u{k} == E2*zk*del{k},...
                                E2*del{k} == E3,...
                                u{k}(3) == z10{k},...
                                ];
    if k > 1
        constraints = [constraints,...
            IE1*del10{k} + IE2*z10{k} <= IE3 + IE4*u{k-1} + IE5*y{k},...
            dumin <= u{k}-u{k-1},...
            u{k}-u{k-1} <= dumax];
    end
                            
    % Dynamics
    constraints = [constraints ,x{k+1} == A*x{k}+B*u{k},...
                                y{k+1} == C*x{k+1}];

    % Add stage cost to total cost
    objective = objective + norm(y{k}-yref,2);
end
constraints = [constraints ,x{1} == xi];

diagnostics = optimize(constraints,objective);
if diagnostics.problem == 0
 disp('Feasible')
    
    for k = disc_sz:-1:1
        time(k)= k-1;
        sol_x(k) = value(x{1,k}(3));
        sol_y(k) = value(y{1,k}(3));
        sol_u(k) = value(u{1,k}(1));
    end
    
    Cost = value(objective);

elseif diagnostics.problem == 1
 disp('Infeasible')
else
 disp('Something else happened')
end

figure(1);
plot(time,sol_x,'b');
hold on;
plot(time,sol_y,'g');
hold on;
plot(time,sol_u,'r');

