% Pseudoinverse computation via norm minimization
% Copyright - Andrei Marchidan

function [u,x] = pinv_cvx(A,B,xref,uref)

n_u = length(B(1,:)); % Number of inputs
n_x = length(xref);   % Number of outputs

cvx_begin
    variable u(n_u,1)
    variable x(n_x,1)
    minimize norm(A*x+B*u)
    subject to
       x(3) == xref(3)
       u >= 0
cvx_end

end
