% This program computes the SCT model for human behavior with regards to
% physical activity.
clear all
close all
clc

% Setting initial conditions for two different scenarios
eta0 = [0;12;7;10;0;0];
eta02 = [0;21;47;48;0;0];

% Solver time limits
tspan = [0,20];

% Computation using Matlab's built-in ode45
[t,soleta] = ode45(@sct_ode, tspan, eta0);
[t2,soleta2] = ode45(@sct_ode2, tspan, eta02);

% Plotting the results
for i = 1:200
    if (i >= 20 && i <= 60) || (i>=80 && i<=120)
        xi_8(i) = 5; 
    else
        xi_8(i) = 0;
    end
    if (i >= 140 && i <= 150) || (i>=160 && i<=170) || (i>=180 && i<=190)
        xi_42(i) = 5; 
    else
        xi_42(i) = 0;
    end
    xi_4(i) = 0;
end

figure();   
subplot(3,2,1) 
plot([1:1:200],xi_4(:),'LineWidth',2)
hold on;
plot([1:1:200],xi_42(:),'LineWidth',1)
title('Internal Cue');
ylim([0,10]);

subplot(3,2,2)
plot([1:1:200],xi_8(:),'LineWidth',2)
hold on;
plot([1:1:200],xi_8(:),'LineWidth',1)
title('External Cue');
ylim([0,10]);

subplot(3,2,3)
plot(t,soleta(:,3),'LineWidth',2)
hold on;
plot(t2,soleta2(:,3),'LineWidth',1)
title('Self-efficacy');
ylim([0, 100]);

subplot(3,2,6)
plot(t,soleta(:,4),'LineWidth',2)
hold on;
plot(t2,soleta2(:,4),'LineWidth',1)
title('Behavior');
ylim([0, 100]);

subplot(3,2,4)
plot(t,soleta(:,6),'LineWidth',2)
hold on;
plot(t2,soleta2(:,6),'LineWidth',1)
title('Cue to action');
ylim([0, 100]);

subplot(3,2,5)
plot(t,soleta(:,2),'LineWidth',2)
hold on;
plot(t2,soleta2(:,2),'LineWidth',1)
title('Outcome Expectancy');
ylim([0, 100]);

