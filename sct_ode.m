function deta_dt = sct_ode(t,eta)
            
    % model constants
    tau1 = 1; tau2 = 1; tau3 = 1; tau4 = 2; tau5 = 1; tau6 = 3;
    gamma_11 = 3; gamma_22 = 1; gamma_32 = 2; gamma_33 = 1; gamma_35 = 1;
    gamma_36 = 1; gamma_57 = 2; gamma_64 = 15; gamma_68 = 15;
    beta_21 = 0.3; beta_31 = 0.5; beta_42 = 0.3; beta_43 = 0.8; beta_54 = 0.3;
    beta_34 = 0.2; beta_25 = 0.3; beta_14 = 0.23; beta_46 = 0.44; beta_45 = 0.1;

    noise_amp = 1;
    
    xi_1 = 0;
    xi_2 = 3; 
    xi_3 = 3; 
    xi_4 = 0;
    xi_5 = 10; 
    xi_6 = noise_amp*rand();
    xi_7 = noise_amp*rand();

    if (t >= 2 && t <= 6) || (t>=8 && t<=12)
        xi_8 = 5; 
    else
        xi_8 = 0;
    end

    deta_dt = zeros(6,1);

    deta_dt(1) = (1/tau1)*(gamma_11 * xi_1 + beta_14*eta(4) - eta(1));
    deta_dt(2) = (1/tau2)*(gamma_22 * xi_2 + beta_21*eta(1) + beta_25 * eta(5) - eta(2));
    deta_dt(3) = (1/tau3)*(gamma_32 * xi_2 + gamma_33 * xi_3 - gamma_35 * xi_5 + gamma_36 * xi_6 + beta_31*eta(1) + beta_34 * eta(4) - eta(3));
    deta_dt(4) = (1/tau4)*(beta_42 * eta(2) + beta_43 * eta(3) + beta_46 * eta(6) + beta_45 * eta(5) - eta(4));
    deta_dt(5) = (1/tau5)*(gamma_57 * xi_7 + beta_54 * eta(4) - eta(5));
    deta_dt(6) = (1/tau6)*(gamma_64 * xi_4 + gamma_68 * xi_8 - eta(6));
            
end