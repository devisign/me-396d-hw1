% Hybrid MPC for an SCT model on adaptive intervention for physical
% activity
clear all
close all
clc
% -------------------------------------------------------------------------
% constant parameters
tau2    = 10; 
tau3    = 30; 
tau4    = 0.8; 
tau5    = 2; 
tau6    = 0.5;

gamma_311   = 0.4; 
gamma_29    = 2.5; 
gamma_57    = 1; 
gamma_510   = 0.6; 
gamma_64    = 1.5;

beta_25     = 0.5; 
beta_34     = 0.2; 
beta_42     = 0.3;
beta_43     = 0.9;
beta_45     = 0.5; 
beta_46     = 0.9;
beta_54     = 0.6;

K_sr        = 0.8;
lambda      = 1;

c_sr        = 1;

% -------------------------------------------------------------------------
% solver settings
h_p         = 20;        % prediction horizon
h_c         = 5;        % control horizon
dt          = 1;        % sampling time (1 day)
disc_sz     = h_p/dt;   % discretized size

n_x         = 5;        % # of states
n_u         = 3;        % # of inputs
n_y         = 4;        % # of outputs

n_u8        = 6;        % # of possible values for u8
n_u9        = 5;        % # of possible values for u9

tol         = 600;      % tolerance in # of daily steps

umin        = [5000,0,0]';
umax        = [10000,500,500]';

ymin        = [0,0,0,0]';
ymax        = [10000,10000,12000,10000]';

dumin       = [-1000, -500, -500]';
dumax       = [1000, 500, 500]';

% -------------------------------------------------------------------------
% initial and desired conditions
xi      = [0;0;5000;0;0];
yref    = [0;0;10000;0];
uref    = [0;0;0];

% Control input sets
z8 = [5000,6000,7000,8000,9000,10000]';
z9 = [100,200,300,400,500]';
zk = [z8;z9];
for i=1:disc_sz
    if i == 1
        zk_aug = zk;
    else
        zk_aug = vertcat(zk_aug,zk);
    end
end
zk_aug = diag(zk_aug);

% -------------------------------------------------------------------------
% coefficient matrices
A = [...
    (1-dt/tau2),        0,                  0,                                      beta_25*dt/tau2,            0;
    0,                  (1-dt/tau3),        beta_34*dt/tau3 + gamma_311*dt/tau3,    0,       0;
    beta_42*dt/tau4,    beta_43*dt/tau4,    (1-dt/tau4),                            beta_45*dt/tau4,            beta_46*dt/tau4;
    0,                  0,                  beta_54*dt/tau5,                        (1-dt/tau5),                0;
    0,                  0,                  0 - gamma_64*c_sr*dt/tau6,              0,  (1-dt/tau6)];

B = [...
    0,                      gamma_29*dt/tau2,   0;
    -gamma_311*dt/tau3,     0,                  0;
    0,                      0,                  0;
    0,                      0,                  gamma_510*dt/tau5;
    gamma_64*c_sr*dt/tau6,  0,                  0];

% -------------------------------------------------------------------------
% Setting up the augmented matrices
C = eye(n_y);
C = horzcat(C,zeros(n_y,1));
D = zeros(n_y,n_u);
sys = ss(A,B,C,D);
sys_d = c2d(sys,dt);
A = sys_d.A;
B = sys_d.B;
C = sys_d.C;
D = sys_d.D;

IE1 = [...
    umin(1)-ymax(3);
    umax(1)-ymin(3);
    umax(2)-umin(3);
    umax(3)-umin(2);
    umin(3);
    -umax(3)];

IE2 = [0,0,-1,1,-1,1]';
IE3 = [...
        0;
        umax(1)-ymin(3);
        umax(2)-umin(3);
        umax(3)-umin(2);
        0;
        0];
IE4 = [1,0,0; -1,0,0; 0,-1,0; 0,1,0; 0,0,0; 0,0,0];
IE5 = [0,0,-1,0; 0,0,1,0; zeros(4)];
IE6 = eye(n_u);
IE7 = eye(n_u);

E1 = [1,0,0;0,1,0];
E2 = [ones(1,n_u8),zeros(1,n_u9); zeros(1,n_u8), ones(1,n_u9)];
E3 = [1;1];

for i = 1:disc_sz
    
    if i == 1
        A_dyn = A;
        B_dyn = B;
        C_dyn = blkdiag(C,C);
        
        y_min  = vertcat(ymin,ymin);
        y_max  = vertcat(ymax,ymax);
        u_min  = umin;
        u_max  = umax;
        
        E1_aug = E1;
        E2_aug = E2;
        E3_aug = E3;
        
        IE6_aug = IE6;
        IE7_aug = IE7;
        
        y_ref  = vertcat(yref,yref);
        u_ref  = uref;
        du_min = dumin;
        du_max = dumax;
    else
        A_dyn = blkdiag(A_dyn,A);
        B_dyn = blkdiag(B_dyn,B);
        C_dyn = blkdiag(C_dyn,C);
        
        y_min = vertcat(y_min, ymin);
        y_max = vertcat(y_max, ymax);
        u_min = vertcat(u_min, umin);
        u_max = vertcat(u_max, umax);
        
        E1_aug = blkdiag(E1_aug,E1);
        E2_aug = blkdiag(E2_aug,E2);
        E3_aug = vertcat(E3_aug,E3);
        
        y_ref = vertcat(y_ref,yref);
        u_ref = vertcat(u_ref,uref);
        du_min = vertcat(du_min,dumin);
        du_max = vertcat(du_max,dumax);
        
        IE6_aug = blkdiag(IE6_aug,IE6);
        IE7_aug = blkdiag(IE7_aug,IE7);
    end    

end
        
IE1_aug = IE1;
IE2_aug = IE2;
IE3_aug = IE3;
IE4_aug = IE4;
IE5_aug = IE5;

for i = 1:disc_sz-1
    IE1_aug = blkdiag(IE1_aug,IE1);
    IE2_aug = blkdiag(IE2_aug,IE2);
    IE3_aug = vertcat(IE3_aug,IE3);
    IE4_aug = blkdiag(IE4_aug,IE4);
    IE5_aug = blkdiag(IE5_aug,IE5);
end
IE5_aug = horzcat(zeros(6*disc_sz,n_y),IE5_aug);

A_dyn = vertcat(zeros(n_x,disc_sz*n_x),A_dyn);
A_dyn = horzcat(A_dyn,zeros((disc_sz+1)*n_x,n_x));
B_dyn = vertcat(zeros(n_x,disc_sz*n_u),B_dyn);

IE7_aug = vertcat(zeros(n_u,disc_sz*n_u),IE7_aug);
IE7_aug = IE7_aug(1:(end-n_u),:);

%-------------------------------------------------------------------------
% In case we would like a more complex cost function, we would need to
% perform a Cholesky decomposition to get the sqrt matrix. This would be
% beneficial especially if Qy is sparse! 

% % [Qy_sqrt,p,S]   = chol( Qy );
% % Qy_sqrt         = Qy_sqrt * S;
Qy_sqrt = eye(n_y*(disc_sz+1));


% -------------------------------------------------------------------------
% Solver
% Ideally here, if the cvx solver would be working, we would uncomment the
% while loop which allows for the prediction and control horizon to update
% the states and controller

cvx_solver Gurobi_2
cvx_save_prefs
y = xi(1:4);
iter = 0;
while iter<=50% (abs(y(3)-yref(3)) >= tol) 
    cvx_precision best
    cvx_begin
        variable x((disc_sz+1)*n_x,1)           % states
        variable u(disc_sz*n_u,1)               % inputs 
        variable y((disc_sz+1)*n_y,1)               % outputs
        variable z(disc_sz*(n_u8+n_u9),1)       % aux for input sets
        variable z10(disc_sz,1)                 % aux for u10
        variable del(disc_sz*(n_u8+n_u9),1) binary  % binary for input sets
        variable del10(disc_sz,1) binary            % binary for if/then

        minimize norm(Qy_sqrt*(y-y_ref))        % penalize daily steps err
        subject to
            x - A_dyn*x - B_dyn*u == [xi;zeros(n_x*disc_sz,1)];
            y - C_dyn*x == zeros((n_y)*(disc_sz+1),1);
%             y_min <= y <= y_max;
%             u_min <= u <= u_max;
%             du_min <= IE6_aug*u - IE7_aug*u <= du_max;
%             E1_aug*u == E2_aug*(zk_aug*del);
%             E2_aug*del == E3_aug;
%             IE1_aug*del10 + IE2_aug*z10 <= IE3_aug + IE4_aug*u + IE5_aug*y;
%             u(3:3:end) == z10; 
    cvx_end
    
    % Setting up new initial conditions for the next prediction horizon
    xi = x((n_x*((h_c/dt)-1))+1:(n_x*((h_c/dt)-0)));

    % Setting up time limits for control horizon
    
    if iter>0
        t0 = tf;
        tf = t0+h_c-1;
    else
        t0 = (iter)*h_c;
        tf = (iter+1)*h_c-1;
    end
    
    t0 = 1;
    tf = h_p;
    % Plotting
    figure(1);
    plot(t0:tf,x(3:5:(n_x*h_p/dt)),'b')
    hold on;
    plot(t0:tf,u(1:3:(n_u*((h_p/dt)))),'r')
    hold on;
    plot(t0:tf,y(3:4:(n_y*h_p/dt)),'--g')
    xlabel('Days');
    ylabel('Daily Steps Performed');
    axis square
    legend('Daily Steps (n_4)','Goals (u_8)','Daily Steps Performed (y_4)');

    figure(2);
    plot(t0:tf,u(2:3:(n_u*((h_p/dt)))),'b')
    hold on;
    plot(t0:tf,u(3:3:(n_u*h_p/dt)),'g')
    xlabel('Days');
    ylabel('Points');
    axis square
    legend('Outcome Expectancy (u_9)','Granted Points (u_{10})');
    pause(1);
    
    iter = iter+1;
end
